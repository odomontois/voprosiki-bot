# Dockerfile for creating a statically-linked Rust application using docker's
# multi-stage build feature. This also leverages the docker build cache to avoid
# re-downloading dependencies if they have not changed.
FROM rust AS build
WORKDIR /usr/src

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN rustup target add x86_64-unknown-linux-musl
RUN apt update
RUN apt install -y musl-tools libssl-dev
ENV DEBIAN_FRONTEND=noninteractive
ENV PKG_CONFIG_ALLOW_CROSS=1
RUN USER=root cargo new vragen
WORKDIR /usr/src/vragen
COPY Cargo.toml Cargo.lock ./
RUN cargo build --target x86_64-unknown-linux-musl --release --features vendored

# Copy the source and build the application.
COPY src ./src
RUN mkdir -p /build-out
RUN touch src/main.rs
RUN cargo build --target x86_64-unknown-linux-musl --release --features vendored
RUN cp target/release/vragen /build-out/

FROM alpine
COPY --from=build /usr/src/vragen/src /src
COPY --from=build /build-out/vragen /

CMD ["/vragen"]