use std::error::Error;

mod bot;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    log::info!("Starting the Voprosiki bot!");

    let db_connection = bot::db::connection().await?;

    bot::run_bot(db_connection).await;

    Ok(())
}
