use aragog::{DatabaseConnection, DatabaseRecord, Record};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use teloxide::types::ChatId;

use crate::bot::Res;

#[derive(Clone, Serialize, Deserialize, Record, Debug)]
pub struct Poll {
    pub title: String,
    pub citizen_candidates: bool,
    pub created: DateTime<Utc>,
    pub end_time: Option<DateTime<Utc>>,
    pub group_restricted: Option<ChatId>,
}

pub type DBPoll = DatabaseRecord<Poll>;

#[derive(Clone, Serialize, Deserialize, Record, Debug)]
pub struct PollOption {
    pub created: DateTime<Utc>,
}

impl Poll {
    pub async fn create(
        db: &DatabaseConnection,
        group_restricted: Option<ChatId>,
        title: String,
    ) -> Res<DBPoll> {
        let created = Utc::now();
        let poll = Poll {
            title,
            created,
            citizen_candidates: true,
            end_time: None,
            group_restricted,
        };
        let db_poll = DatabaseRecord::create(poll, db).await?;
        Ok(db_poll)
    }
}
