use aragog::{DatabaseConnection, DatabaseRecord, EdgeRecord, Record};
use serde::{Deserialize, Serialize};
use teloxide::types::User;

use crate::bot::Res;

#[derive(Clone, Serialize, Deserialize, Record)]
pub struct Citizen {
    pub name: Option<String>,
    pub align: Option<String>,
}

pub type DBCitizen = DatabaseRecord<Citizen>;

#[derive(Clone, Serialize, Deserialize, Record)]
pub struct Creator {}

#[allow(dead_code)]
pub type DBCreator = DatabaseRecord<EdgeRecord<Creator>>;

impl Citizen {
    pub async fn upsert(db: &DatabaseConnection, user: &User) -> Res<DBCitizen> {
        let search = Self::upsert_aql(&user.id.0.to_string(), &user.full_name());
        let q = DatabaseRecord::<Citizen>::aql_get(&search, db).await?;
        Ok(q.uniq()?)
    }

    fn upsert_aql(id: &str, name: &str) -> String {
        let collection = Self::COLLECTION_NAME;
        format!(
            "UPSERT {{_key: '{id}' }} 
             INSERT {{_key: '{id}', name: '{name}'}}
             UPDATE {{name: '{name}'}}
             IN {collection}
             RETURN NEW"
        )
    }

    pub async fn link_author<A: Record>(
        author: Option<&User>,
        other: &DatabaseRecord<A>,
        db: &DatabaseConnection,
    ) -> Res<()> {
        if let Some(author) = author {
            let citizen = Self::upsert(db, author).await?;
            DatabaseRecord::link(&citizen, other, db, Creator {}).await?;
        }
        Ok(())
    }

    pub fn id(user: &User) -> String {
        user.id.0.to_string()
    }
}
