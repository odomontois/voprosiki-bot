mod command;
pub mod db;
mod handle;

use aragog::DatabaseConnection;
use teloxide::{prelude::*, utils::command::BotCommands};

use handle::handle_command;

use crate::bot::handle::handle_callback;

use self::command::Command;

type Error = Box<dyn std::error::Error + Sync + Send>;
pub type Res<A> = Result<A, Error>;

pub async fn run_bot(db_connection: DatabaseConnection) {
    let bot = Bot::from_env().auto_send();

    log::info!("starting!");

    bot.set_my_commands(Command::bot_commands()).await.unwrap();

    log::info!("started!");

    let command_handler = Update::filter_message()
        .filter_command::<Command>()
        .endpoint(handle_command);

    let callback_handler = Update::filter_callback_query().endpoint(handle_callback);

    let handler = dptree::entry()
        .branch(command_handler)
        .branch(callback_handler);

    Dispatcher::builder(bot, handler)
        .enable_ctrlc_handler()
        .dependencies(dptree::deps![db_connection])
        .build()
        .dispatch()
        .await
}
