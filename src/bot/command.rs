use serde::{Deserialize, Serialize};
use teloxide::utils::command::BotCommands;

#[derive(BotCommands, Clone)]
#[command(rename = "snake_case", description = "These commands are supported:")]
pub enum Command {
    #[command(description = "set political align")]
    SetAlign { align: String },
    #[command(description = "get political align")]
    GetAlign,
    #[command(description = "create a new poll")]
    CreatePoll { title: String },
}

#[derive(Serialize, Deserialize)]
pub enum CallBack {
    PollSelfNomination { id: String },
    PollDelete { id: String },
}
