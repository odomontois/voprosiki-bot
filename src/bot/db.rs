pub mod citizen;
pub mod poll;

use aragog::DatabaseConnection;

pub async fn connection() -> Result<DatabaseConnection, impl std::error::Error> {
    DatabaseConnection::builder()
        // The schema wil silently apply to the database, useful only if you don't use the CLI and migrations
        .apply_schema()
        // You then need to build the connection
        .build()
        .await
}
