mod align;
mod poll;

use aragog::DatabaseConnection;

use crate::bot::handle::align::handle_set_align;

use self::{
    align::handle_get_align,
    poll::{delete_poll, nominate_self},
};

use super::{
    command::{CallBack, Command},
    Res,
};

use teloxide::prelude::*;

pub struct CommandContext {
    pub bot: AutoSend<Bot>,
    pub db: DatabaseConnection,
    pub message: Message,
}

pub async fn handle_command(
    bot: AutoSend<Bot>,
    db: DatabaseConnection,
    message: Message,
    command: Command,
) -> Res<()> {
    let context = CommandContext { bot, db, message };
    match command {
        Command::SetAlign { align } => handle_set_align(context, align).await,
        Command::GetAlign => handle_get_align(context).await,
        Command::CreatePoll { title } => poll::create_poll(context, title).await,
    }
}

pub struct CallbackContext {
    pub bot: AutoSend<Bot>,
    pub db: DatabaseConnection,
    pub query: CallbackQuery,
}

pub async fn handle_callback(
    query: CallbackQuery,
    bot: AutoSend<Bot>,
    db: DatabaseConnection,
) -> Res<()> {
    let callback_data = query.data.as_ref().ok_or("no callback data")?;
    let callback: CallBack = serde_json::from_str(callback_data)?;
    let context = CallbackContext { bot, db, query };
    match callback {
        CallBack::PollDelete { id } => delete_poll(context, id).await,
        CallBack::PollSelfNomination { id } => nominate_self(context, id).await,
    }
}
