use crate::bot::{
    command::CallBack,
    db::poll::Poll,
    db::{citizen::Citizen, poll::DBPoll},
    Res,
};

use teloxide::{
    prelude::*,
    types::{InlineKeyboardButton, InlineKeyboardMarkup, ReplyMarkup},
};

use super::{CallbackContext, CommandContext};

pub async fn create_poll(
    CommandContext { bot, db, message }: CommandContext,
    title: String,
) -> Res<()> {
    let reply_text = format!("Poll {title} created");
    let poll = Poll::create(&db, Some(message.chat.id), title).await?;

    Citizen::link_author(message.from(), &poll, &db).await?;

    let keys = ReplyMarkup::InlineKeyboard(poll_keyboard(&poll)?);

    bot.send_message(message.chat.id, &reply_text)
        .reply_markup(keys)
        .reply_to_message_id(message.id)
        .await?;

    Ok(())
}

#[allow(unused_variables)]
pub async fn nominate_self(
    CallbackContext { bot, db, query }: CallbackContext,
    poll_id: String,
) -> Res<()> {
    Ok(())
}

#[allow(unused_variables)]
pub async fn delete_poll(
    CallbackContext { bot, db, query }: CallbackContext,
    poll_id: String,
) -> Res<()> {
    bot.answer_callback_query(query.id).text("insufficient rights for deleting").await?;
    Ok(())
}

fn callback_button(text: &str, data: CallBack) -> Res<InlineKeyboardButton> {
    let callback_data = serde_json::to_string(&data)?;
    Ok(InlineKeyboardButton::callback(text, callback_data))
}

fn poll_keyboard(poll: &DBPoll) -> Res<InlineKeyboardMarkup> {
    let nominate = CallBack::PollSelfNomination {
        id: poll.key().clone(),
    };
    let delete = CallBack::PollDelete {
        id: poll.key().clone(),
    };
    let nominate_button = callback_button("Nominate MySelf", nominate)?;
    let delete_button = callback_button("Delete the poll", delete)?;
    Ok(InlineKeyboardMarkup::new([[
        nominate_button,
        delete_button,
    ]]))
}
