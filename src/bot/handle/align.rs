use crate::bot::{db::citizen::Citizen, Res};
use aragog::DatabaseRecord;
use teloxide::{prelude::*, types::User};

use super::CommandContext;

pub async fn handle_set_align(
    CommandContext { message, db, bot }: CommandContext,
    align: String,
) -> Res<()> {
    if align.is_empty() {
        bot.send_message(message.chat.id, "please specify your alignment")
            .await?;
        return Ok(());
    }

    let user_id = message.from().ok_or("anon user")?;
    let mut citizen = Citizen::upsert(&db, user_id).await?;

    citizen.record.align = Some(align.clone());
    citizen.save(&db).await?;

    bot.send_message(
        message.chat.id,
        format!("Your political aligment was set to {align}"),
    )
    .await?;

    //sdfsfddsf

    Ok(())
}

pub async fn handle_get_align(CommandContext { db, message, bot }: CommandContext) -> Res<()> {
    let chat_id = message.chat.id;
    let (user, text) = get_user_and_text(&message).ok_or("can't define user")?;

    let id = Citizen::id(user);
    let citizen = DatabaseRecord::<Citizen>::find(&id, &db).await?;
    let result = if let Some(align) = &citizen.align {
        format!("{text} allign is '{align}'")
    } else {
        "You haven't set align yet".to_string()
    };

    bot.send_message(chat_id, result)
        .reply_to_message_id(message.id)
        .await?;

    Ok(())
}

fn get_user_and_text(message: &Message) -> Option<(&User, String)> {
    Some(if let Some(reply) = message.reply_to_message() {
        let user = reply.from()?;
        let name = &user.first_name;
        (user, format!("{name}'s"))
    } else {
        let user = message.from()?;
        (user, "Your".to_string())
    })
}
